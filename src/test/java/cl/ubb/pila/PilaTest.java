package cl.ubb.pila;

import static org.junit.Assert.*;

import org.junit.Test;

public class PilaTest {

	@Test
	public void nuevaPilaVacio() {
		//arrange
		Pila pila = new Pila();
		int largo;
		//act
		largo=pila.size();
		//assert
		assertEquals(largo,0);
	}
	
	@Test
	public void apilarUnoEnPilaNoVacia() {
		//arrange
		Pila pila = new Pila();
		//act
		pila.apilar("5");//stack no vacia, ademas prueba agregar numero en pila vacia(no va al caso)
		pila.apilar("1");//se le agrega el 1
		//assert
		assertEquals(pila.get(pila.size()-1),"1");//size por que deberia estar al final
	}
	@Test
	public void apilarUnoYDosEnPilaNoVacia() {
		//arrange
		Pila pila = new Pila();
		//act
		pila.apilar("5");//stack no vacia, ademas prueba agregar numero en pila vacia(no va al caso)
		pila.apilar("1");//se le agrega el 1 y 2
		pila.apilar("2");
		//assert
		assertEquals(pila.get(pila.size()-2),"1");//size por que deberia estar al final
		assertEquals(pila.get(pila.size()-1),"2");
	}
}

